// normal link
{/* <Link to="/gotoA">Home</Link> */}

// link which highlights currently active route with the given class name
{/* <NavLink to="/gotoB" activeClassName="active">
  React
</NavLink> */}

// you can redirect to this url
{/* <Redirect to="/gotoC" /> */}