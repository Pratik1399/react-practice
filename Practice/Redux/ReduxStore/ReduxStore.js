
// import connect
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// import action creators
import * as userActions from '../../../actions/userActions';

export class User extends React.Component {

    handleSubmit() {
        // dispatch an action
        this.props.actions.sendEmail(this.state.email);
    }
}

// you are mapping you state props
const mapStateToProps = (state, ownProps) => ({ user: state.user })
// you are binding your action creators to your props
const mapDispatchToProps = (dispatch) => ({ actions: bindActionCreators(userActions, dispatch) })

export default connect(mapStateToProps, mapDispatchToProps)(User);