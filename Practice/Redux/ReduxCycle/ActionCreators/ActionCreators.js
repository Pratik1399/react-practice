// action creator

export function sendEmail(data) {
    return { type: "SEND_EMAIL", payload: data };
}