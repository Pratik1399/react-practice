import React from 'react';
import '../App.css';

export class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "some name"
        }
    }

    render() {
        //reading state
        const name = this.state.name;
        //reading props
        const address = this.props.address;
        return (
            <div className="dashboard">
                {name}
                {address}
            </div>
        );
    }
}