import React from 'react';
import '../App.css';
import { ToDoForm } from './todoform';
import { ToDolist } from './todolist';
import { ErrorBoundary } from '../errorboundary';

export class Dashboard extends React.Component {
    render() {
        return (
            <div className="dashboard">
                <ErrorBoundary>
                    <ToDoForm />
                    <ToDolist />
                </ErrorBoundary>
            </div>
        );
    }
}