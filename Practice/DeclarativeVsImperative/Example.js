const numbers = [1, 2, 3, 4, 5];

//console.log(numbers,"numbers");

//declarative programming

const doubleWithDec = numbers.map(number => number * 2);

console.log(doubleWithDec, "doubleWithDec");

//imperative programming

const doubleWithImp = [];

//console.log(doubleWithImp,"doubleWithImp");

for (let i = 0; i < numbers.length; i++) {
    const numberdouble = numbers[i] * 2;

    //console.log(numberdouble,"numberdouble");

    doubleWithImp.push(numberdouble);
}

console.log(doubleWithImp, "doubleWithImp");