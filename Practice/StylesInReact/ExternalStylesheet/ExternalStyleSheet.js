import React from 'react';
import './App.css';
import { Header } from './header/header';
import { Footer } from './footer/footer';
import { Dashboard } from './dashboard/dashboard';
import { UserDisplay } from './userdisplay';

function App() {
    return (
        <div className="App">
            <Header />
            <Dashboard />
            <UserDisplay />
            <Footer />
        </div>
    );
}

export default App;