//Data Transformation


let cities=["irving","lowell","houston"];

console.log(cities,"cities");

// we can get the comma separated list
console.log(cities.join(','),"cities.join");
// irving,lowell,houston

// if we want to get cities start with i
const citiesI=cities.filter(city=>city[0]==="i");

console.log(citiesI,"citiesI");
// [ 'irving' ]

// if we want to capitalize all the cities
const citiesC=cities.map(city=>city.toUpperCase());

console.log(citiesC,"citiesC");
// [ 'IRVING', 'LOWELL', 'HOUSTON' ]