//Pure Vs Impure Function

let student = {
    firstName: 'testing',
    lastName: 'testing',
    marks: 500
}

//console.log(student, "student");

//Impure Function

function appendAddress() {
    student.address = {
        streetNumber: "0000",
        streetName: "first",
        city: "somecity"
    }
}

//console.log(appendAddress());

//Pure Function

function appendAddress(student) {
    let copystudent = Object.assign({}, student);
    copystudent.address = {
        streetNumber: "0000",
        streetName: "first",
        city: "somecity"
    }
    return copystudent;
}

console.log(appendAddress(student), "appendAddress");

console.log(student, "student");