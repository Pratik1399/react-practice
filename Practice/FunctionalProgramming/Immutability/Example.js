//Immutability Demonstration

let student = {
    firstName: 'testing',
    lastName: 'testing',
    marks: 500
}

// console.log(student, "student");

function changeName(student) {
    // student.firstName="testing11" //Wrong way

    let copiedStudent = Object.assign({}, student);

    // console.log(copiedStudent, "copiedStudent");

    copiedStudent.firstName = "testing11";

    return copiedStudent;
}

console.log(changeName(student), "changeName");

console.log(student, "student")