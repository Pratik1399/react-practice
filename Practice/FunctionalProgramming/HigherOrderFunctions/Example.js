//Higher Order Functions

const numbers = [10, 20, 40, 50, 60, 70, 80];

const out1 = numbers.map(num => num * 100);

console.log(out1, "out1");

const out2 = numbers.filter(num => num > 50);

console.log(out2, "out2");

const out3 = numbers.reduce((out, num) => out + num);

console.log(out3, "out3");

const isYoung = age => age < 25;

const message = msg => "He is " + msg;

function isPersonOld(age, isYoung, message) {
    const returnMessage = isYoung(age) ? message("young") : message("old");
    return returnMessage;
}

//passing functions as arguments
console.log(isPersonOld(13, isYoung, message));
//He is Young