//Recursion

function printMyName(name,count) {
    if(count<=name.length){
        console.log(name.substring(0,count));
        printMyName(name,++count)
    }
}

console.log(printMyName("Pratik", 1));

//without recursion

var name="Pratik";
var output="";

for(let i=0;i<name.length;i++){
    output=output+name[i];
    console.log(output);
}