// Without Fragments   
return (
    <div>
        <CompoentA />
        <CompoentB />
        <CompoentC />
    </div>
)

// With Fragments   
return (
    <React.Fragment>
        <CompoentA />
        <CompoentB />
        <CompoentC />
    </React.Fragment>
)

// shorthand notation Fragments   
return (
    <>
        <CompoentA />
        <CompoentB />
        <CompoentC />
    </>
)