import apiservice from '../services/apiservice';

export function sendEmail(data) {
    return { type: "SEND_EMAIL", payload: data };
}

export function sendEmailAPI(email) {
    return function (dispatch) {
        return apiservice.callAPI(email).then(data => {
            dispatch(sendEmail(data));
        });
    }
}