
import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export class ToDoForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.input = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.input.current.value);
        event.preventDefault();
    }

    render() {
        return (
            <div className="todoform">
                <Form>
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                            <span className="item">Item</span>
                        </Form.Label>
                        <Col sm={5}>
                            <Form.Control type="text" placeholder="Todo Item" ref={this.input} />
                        </Col>
                        <Col sm={5}>
                            <Button variant="primary" onClick={this.handleSubmit} type="submit">Add</Button>
                        </Col>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}