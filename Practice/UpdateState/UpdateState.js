//  wrong way
this.state.name = "some name";

// right way
this.setState({ name: "some name" });

// wrong way
this.setState({
    timesVisited: this.state.timesVisited + this.props.count
})
// right way
this.setState((state, props) => {
    timesVisited: state.timesVisited + props.count
});